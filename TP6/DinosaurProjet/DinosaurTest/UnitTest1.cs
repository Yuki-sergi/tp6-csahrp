
using System;
using Xunit;
using System.Collections.Generic;
using DinosaurConsole;

namespace DinosaurTest
{
    public class UnitTest1
    {
        [Fact]
        public void TestDinosaurConstructor()
        {
            Dinosaur louis = new Dinosaur("Louis", "Stegausaurus", 12);

            Assert.Equal("Louis", louis.getName());
            Assert.Equal("Stegausaurus", louis.getSpecie());
            Assert.Equal(12, louis.getAge());
        }

        [Fact]
        public void TestDinosaurRoar()
        {
            Dinosaur louis = new Dinosaur("Louis", "Stegausaurus", 12);
            Assert.Equal("Grrr", louis.roar());
        }

        [Fact]
        public void TestDinosaurSayHello()
        {
            Dinosaur louis = new Dinosaur("Louis", "Stegausaurus", 12);
            Assert.Equal("Je suis Louis le Stegausaurus, j'ai 12 ans.", louis.sayHello());
        }

        [Fact]
        public void TestDinosaurHug()
        {
            Dinosaur louis = new Dinosaur("Louis", "Stegausaurus", 12);
            Dinosaur theo = new Dinosaur("Theo", "Bretonorus", 54);
            Assert.Equal("Je suis Louis et je fais un calin à Theo.", louis.hug(theo));
        }
    }
}
