
using System;
using System.Collections.Generic;
namespace DinosaurConsole
{
    public class Dinosaur
    {
        private string name;
        private string specie;
        private int age;

        public string getName()
        {
            return this.name;
        }
        public string getSpecie()
        {
            return this.specie;
        }
        public int getAge()
        {
            return this.age;
        }
        public void setName(string name)
        {
            this.name=name;
        }
        public void setSpecie(string specie)
        {
            this.specie=specie;
        }
        public void setAge(int age)
        {
            this.age=age;
        }

        public Dinosaur(string name, string specie, int age)
        {
            setName(name);
            setSpecie(specie);
            setAge(age);
        }

        public string sayHello()
        {
            return string.Format("Je suis {0} le {1}, j'ai {2} ans.", 
            getName(), getSpecie(), getAge());
        }

        public string roar()
        {
            return "Grrr";
        }

        public string hug(Dinosaur dinosaur)
        {
            return String.Format("Je suis {0} et je fais un calin à {1}.",
            getName(), dinosaur.getName());
        }
    }

    public class Horde
    {
        List<Dinosaur> Members = new List<Dinosaur>();  
        

        public void présentaionDino()
        {
            foreach (Dinosaur dino in Members )
            {
                Console.WriteLine( String.Format("Je suis {0} je suis de l'espèce {1} et j'ai {3}.",
                dino.getName(), dino.getSpecie(), dino.getAge()));
            }

        }

        public void Add(Dinosaur dino)
        {
            Members.Add(dino);
        }

        
    }   
 
}