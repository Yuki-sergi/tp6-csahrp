﻿
using System;
using System.Collections.Generic;
namespace DinosaurConsole
{
    public class Program
    {
        static void Main(string[] args)
        {
            Dinosaur louis = new Dinosaur("Louis", "Stegosaurus", 12);
            Dinosaur nessie = new Dinosaur("Nessie", "Diplodocus", 11);
            Dinosaur Felix = new Dinosaur("Felix", "Catogirlus", 25);
            Dinosaur Mikan = new Dinosaur("Mikan", "Nursocutus", 18);
            List<Dinosaur> dinosaurs = new List<Dinosaur>();

            dinosaurs.Add(louis);
            dinosaurs.Add(nessie);
            dinosaurs.Add(Felix);
            dinosaurs.Add(Mikan);

            Horde groupe = new Horde();
            
            groupe.Add(louis);
            groupe.Add(nessie);
            groupe.Add(Felix);
            groupe.Add(Mikan);
            
            Console.WriteLine(dinosaurs.Count);

            foreach (Dinosaur dino in dinosaurs)
            {
                Console.WriteLine(dino.sayHello());
            }

            dinosaurs.RemoveAt(1);

            Console.WriteLine(dinosaurs.Count);

            foreach (Dinosaur dino in dinosaurs)
            {
                Console.WriteLine(dino.sayHello());
            }

            dinosaurs.Remove(louis);

            Console.WriteLine(dinosaurs.Count);

            foreach (Dinosaur dino in dinosaurs)
            {
                Console.WriteLine(dino.sayHello());
            }
        }
    }
}
