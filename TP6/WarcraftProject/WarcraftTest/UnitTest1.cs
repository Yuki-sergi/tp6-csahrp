//Made by Xzing
using System;
using Xunit;

using WarcraftConsole;

namespace WarcraftTest
{
    public class UnitTest1
    {
        [Fact]
        public void TestUnitConstructor1()
        {
            Unit peasant0 = new Unit("Peasant", 30, 0);

            Assert.Equal("Peasant",peasant0.getType());
            Assert.Equal("Human",peasant0.getRace());
            Assert.Equal("Alliance",peasant0.getFaction());
            Assert.Equal(30,peasant0.getHP());
            Assert.Equal(0,peasant0.getArmor());
        }
        [Fact]
        public void TestUnitConstructor2()
        {
            Unit peon0 = new Unit("Peon", 30, 0);

            Assert.Equal("Peon",peon0.getType());
            Assert.Equal("Orc",peon0.getRace());
            Assert.Equal("Horde",peon0.getFaction());
            Assert.Equal(30,peon0.getHP());
            Assert.Equal(0,peon0.getArmor());
        }
        [Fact]
        public void TestSayHello()
        {
            Unit peasant0 = new Unit("Peasant", 30, 0);
            Unit peon0 = new Unit("Peon", 30, 0);
            Assert.Equal("Hello",peasant0.sayHello());            
            Assert.Null(peon0.sayHello());
        }
        [Fact]
        public void TestGrunt()
        {
            Unit peasant0 = new Unit("Peasant", 30, 0);
            Unit peon0 = new Unit("Peon", 30, 0);
            Assert.Equal("Grrr",peon0.grunt());            
            Assert.Null(peasant0.grunt());
        }
        [Fact]
        public void TestTalk()
        {
            Unit peasant0 = new Unit("Peasant", 30, 0);
            Unit peon0 = new Unit("Peon", 30, 0);
            Assert.Equal("Encore du travail ?",peasant0.talk());            
            Assert.Equal("Ready to work !",peon0.talk());
        }
        [Fact]
        public void TestTalktoPeasant()
        {
            Unit peasant0 = new Unit("Peasant", 30, 0);
            Unit peon0 = new Unit("Peon", 30, 0);
            Unit peasant1 = new Unit("Peasant", 30, 0);
            Assert.Equal("Nous sommes aliés.",peasant0.talkToPeasant(peasant1));
            Assert.Equal("Un ennemi !",peon0.talkToPeasant(peasant1));
        }
        [Fact]
        public void TestTalktoPeon()
        {
            Unit peasant0 = new Unit("Peasant", 30, 0);
            Unit peon0 = new Unit("Peon", 30, 0);
            Unit peon1 = new Unit("Peon", 30, 0);
            Assert.Equal("C'est une bonne situation ça, humain ?",peasant0.talkToPeon(peon1));
            Assert.Equal("Agrougrou !",peon0.talkToPeon(peon1));            
        }
    }
}
