//Made by Xzing
using System;

namespace WarcraftConsole
{
    public class Unit
    {
        private string type;
        private string race;
        private string faction;
        private int HP;
        private int armor;

        public string getType()
        {
            return this.type;
        }
        public string getRace()
        {
            return this.race;
        }
        public string getFaction()
        {
            return this.faction;
        }
        public int getHP()
        {
            return this.HP;
        }
        public int getArmor()
        {
            return this.armor;
        }

        public void setType(string type)
        {
            this.type=type;
        }
        public void setRace(string type)
        {
            if (type=="Peasant")
            {
                this.race="Human";
            }
            if (type=="Peon")
            {
                this.race="Orc";
            }
        }
        public void setFaction(string race)
        {
            if (race=="Human")
            {
                this.faction="Alliance";
            }
            if (race=="Orc")
            {
                this.faction="Horde";
            }
        }
        public void setHP(int HP)
        {
            this.HP=HP;
        }
        public void setArmor(int armor)
        {
            this.armor=armor;
        }

        public Unit(string type, int HP, int armor)
        {
            setType(type);
            setRace(type);
            setFaction(race);
            setHP(HP);
            setArmor(armor);            
        }

        public string sayHello()
        {
            if (getRace()=="Human")
            {
                return "Hello";
            }
            return null;
        }
        public string grunt()
        {
            if (getRace()=="Orc")
            {
                return "Grrr";
            }
            return null;
        }
        public string talk()
        {
            if (getRace()=="Orc")
            {
                return "Ready to work !";
            }
            if (getRace()=="Human")
            {
                return "Encore du travail ?";
            }
            return null;
        }
        public string talkToPeasant(Unit peasant)
        {
            if (getRace()=="Orc")
            {
                return "Un ennemi !";
            }
            if (getRace()=="Human")
            {
                return "Nous sommes aliés.";
            }
            return null;
        }
        public string talkToPeon(Unit peon)
        {
            if (getRace()=="Orc")
            {
                return "Agrougrou !";
            }
            if (getRace()=="Human")
            {
                return "C'est une bonne situation ça, humain ?";
            }
            return null;
        }
    }
}